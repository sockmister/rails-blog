def mv(src, dest)
  File.delete(dest)
  File.rename(src, dest)
end

def add(hostname, ip_addr)
  str = ip_addr + " " + hostname
  puts str
  open('hosts', 'a') do |f|
      f << ip_addr + " " + hostname 
  end
end

def del(hostname)
  tmp = File.open("hosts.tmp", 'w')
  open('hosts', 'r') do |f|
    f.each do |line|
      tokens =  line.split(" ")
      unless tokens.include?(hostname)
        puts line
        tmp << line
      end
    end
    tmp.close
  end
  mv('hosts.tmp', 'hosts')
end

def replace(hostname, ip_addr)
  del(hostname)
  add(hostname, ip_addr)
end

hostname = ARGV[0]
ip_addr = ARGV[1]

replace(hostname, ip_addr)

